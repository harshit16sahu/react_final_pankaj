import React from 'react';
import { Route, Switch } from 'react-router-dom';


import Rooms from './pages/RoomFinal';
import Error from './pages/Error';
import Profile from './pages/Profile'
import LandingPage from './pages/LandingPage';
import Hotel from './pages/Hotel';
import AboutUs from './pages/AboutUs'
import TOS from './pages/TOS';
import PrivacyPolicy from './pages/PrivacyPolicy';
import ContactUs from './pages/ContactUs';
import CompletePayment from './pages/CompletePayment';
import LoginAndSignup from './pages/LoginAndSignup'
import './App.css';

function App() {
  return (
    <>

      <Switch>

        <Route exact path='/rooms' component={Rooms} />
        <Route exact path='/login' component={LoginAndSignup} />
        <Route exact path='/signup' component={LoginAndSignup} />
        <Route exact path='/profile' component={Profile} />
        <Route exact path='/' component={LandingPage} />
        <Route exact path="/Hotel/:slug" component={Hotel} />
        <Route exact path="/AboutUs" component={AboutUs} />
        <Route exact path="/ContactUs" component={ContactUs} />
        <Route exact path="/termsandconditions" component={TOS} />
        <Route exact path="/PrivacyPolicy" component={PrivacyPolicy} />
        <Route exact path="/CompletePayment" component={CompletePayment} />

        <Route component={Error} />
      </Switch>

    </>
  );
}

export default App;
