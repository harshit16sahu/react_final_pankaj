import React, { Component } from "react";
import "./../pages/LandingPage.css";


export default class LPImage extends Component {
	render() {
		return (
			<div
				className="LPRowImage"
				style={{ backgroundImage: `url( ${this.props.url} )` }}
			>
				<div
					className="LPImageBlack"
					onClick={() => this.props.handleClick(this.props.text)}
				>
					{this.props.text}
				</div>
			</div>
		);
	}
}
