import React from "react";
import ReactDOM from "react-dom";
import "antd/dist/antd.css";
import "./Checkbox_component.css";
import { Checkbox } from "antd";

const CheckboxGroup = Checkbox.Group;

class Check_box_grid extends React.Component {
  // onChange = checkedList => {
  //   this.props.setCheckboxGroup(checkedList)
  //   this.setState({
  //     checkedList,
  //     indeterminate: !!checkedList.length && checkedList.length < plainOptions.length,
  //     checkAll: checkedList.length === plainOptions.length,
  //   });
  // };

  onCheckStar = (event) => {
    if (
      event.target.checked &&
      this.props.CheckboxStars.indexOf(event.target.name) === -1
    ) {
      this.props.setCheckboxStars([
        ...this.props.CheckboxStars,
        event.target.name,
      ]);
    } else if (!event.target.checked) {
      this.props.setCheckboxStars([
        ...this.props.CheckboxStars.filter((val) => val !== event.target.name),
      ]);
    }
  };

  onCheckPopular = (event) => {
    if (
      event.target.checked &&
      this.props.CheckboxPopular.indexOf(event.target.name) === -1
    ) {
      this.props.setCheckboxPopular([
        ...this.props.CheckboxPopular,
        event.target.name,
      ]);
    } else if (!event.target.checked) {
      this.props.setCheckboxPopular([
        ...this.props.CheckboxPopular.filter((val) => val !== event.target.name),
      ]);
    }
  };

  // onCheckOptions = (event) => {
  //   if (
  //     event.target.checked &&
  //     this.props.CheckboxOptions.indexOf(event.target.name) === -1
  //   ) {
  //     this.props.setCheckboxOptions([
  //       ...this.props.CheckboxOptions,
  //       event.target.name,
  //     ]);
  //   } else if (!event.target.checked) {
  //     this.props.setCheckboxOptions([
  //       ...this.props.CheckboxOptions.filter((val) => val !== event.target.name),
  //     ]);
  //   }
  // };

  render() {
    return (
      <div className="checkboxholder">
        <div style={{ fontSize: "14px", color: "black" }}>Star ratings</div>
        <br />

        <label class="checkboxcontainer">
          <div style={{ display: "flex", justifyContent: "space-between" }}>
            <div className="checkbox">★ ★ ★ ★ ★</div>
            <div>
              {this.props.filtersObject.star5 ?
                `(${this.props.filtersObject.star5})` : ""}
            </div>
          </div>
          <input type="checkbox" name="star5" onChange={this.onCheckStar} />
          <span className="checkboxcheckmark" />
        </label>

        <label class="checkboxcontainer">
          <div style={{ display: "flex", justifyContent: "space-between" }}>
            <div className="checkbox">★ ★ ★ ★</div>
            <div>
              {this.props.filtersObject.star4 ?
                `(${this.props.filtersObject.star4})` : ""}
            </div>
          </div>
          <input type="checkbox" name="star4" onChange={this.onCheckStar} />
          <span class="checkboxcheckmark" />
        </label>

        <label class="checkboxcontainer">
          <div style={{ display: "flex", justifyContent: "space-between" }}>
            <div className="checkbox">★ ★ ★</div>
            <div>
              {this.props.filtersObject.star3 ?
                `(${this.props.filtersObject.star3})` : ""}
            </div>
          </div>
          <input type="checkbox" name="star3" onChange={this.onCheckStar} />
          <span class="checkboxcheckmark" />
        </label>

        <label class="checkboxcontainer">
          <div style={{ display: "flex", justifyContent: "space-between" }}>
            <div className="checkbox">★ ★</div>
            <div>
              {this.props.filtersObject.star2
                ? `(${this.props.filtersObject.star2})`
                : ""}
            </div>
          </div>
          <input type="checkbox" name="star2" onChange={this.onCheckStar} />
          <span class="checkboxcheckmark" />
        </label>

        <label class="checkboxcontainer">
          <div style={{ display: "flex", justifyContent: "space-between" }}>
            <div className="checkbox">★</div>
            <div>
              {this.props.filtersObject.star1 ?
                `(${this.props.filtersObject.star1})` : ""}
            </div>
          </div>
          <input type="checkbox" name="star1" onChange={this.onCheckStar} />
          <span class="checkboxcheckmark" />
        </label>

        <br />
        <div style={{ fontSize: "14px", color: "black" }}>Popular Filters</div>

                <br/>
            {this.props.hotelType && Object.keys(this.props.hotelType).map( type => (
              <label class="checkboxcontainer" key={type}>
              <div style={{ display: "flex", justifyContent: "space-between" }}>
                <div>{type}</div>
                <div>
                  {this.props.hotelType[type] ?
                    `(${this.props.hotelType[type]})` : ""}
                </div>
              </div>
              <input type="checkbox" name={type} onChange={this.onCheckPopular} />
              <span class="checkboxcheckmark" />
            </label>
            ))}



            {this.props.options.freeBreakfast && <label class="checkboxcontainer">
              <div style={{ display: "flex", justifyContent: "space-between" }}>
                <div>Free Breakfast</div>
                <div>
                  {this.props.options.freeBreakfast ?
                    `(${this.props.options.freeBreakfast})` : ""}
                </div>
              </div>
              <input type="checkbox" name="freeBreakfast" onChange={this.onCheckPopular} />
              <span class="checkboxcheckmark" />
            </label>}

            {this.props.options.payAtHotel && <label class="checkboxcontainer">
              <div style={{ display: "flex", justifyContent: "space-between" }}>
                <div>Pay At Hotel</div>
                <div>
                  {this.props.options.payAtHotel ?
                    `(${this.props.options.payAtHotel})` : ""}
                </div>
              </div>
              <input type="checkbox" name="payAtHotel" onChange={this.onCheckPopular} />
              <span class="checkboxcheckmark" />
            </label>}

            {this.props.options.freeCancellation && <label class="checkboxcontainer">
              <div style={{ display: "flex", justifyContent: "space-between" }}>
                <div>Free Cancellation</div>
                <div>
                  {this.props.options.freeCancellation ?
                    `(${this.props.options.freeCancellation})` : ""}
                </div>
              </div>
              <input type="checkbox" name="freeCancellation" onChange={this.onCheckPopular} />
              <span class="checkboxcheckmark" />
            </label>}




        

        <br />
      </div>
    );
  }
}

export default Check_box_grid;
