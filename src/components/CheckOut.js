import React from 'react'
import "./CheckInOut.css"
export const CheckOut = ({info}) => {
    return (
        <div className="checkInOut">
            <div className="heading">Check Out Information</div>
            <div>
                Check Out Time -  {info.time}
            </div>
        </div>
            
    )
}
