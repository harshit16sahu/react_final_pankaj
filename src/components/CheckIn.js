import React from 'react'
import "./CheckInOut.css"
export const CheckIn = ({info}) => {
    return (
        <div className="checkInOut">
            <div className="heading">Check In Information</div>
            <div>
                Check In between {info.beginTime} and {info.endTime}
            </div>
            <br />
            {info.instructions && 
                <div>
                    <div>Instructions</div>
                    <div>{info.instructions}</div>
                </div>
            }

            <br />
            {info.specialInstructions && 
                <div>
                    <div>Special Instructions</div>
                    <div>{info.instructions}</div>
                </div>
            }
            
        </div>
    )
}
