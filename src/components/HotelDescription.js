import React from 'react'
import "./HotelDescription.css"

export const HotelDescription = (props) => {
    const { descriptions } = props
    const descriptionWithoutAttractions  = descriptions.reduce((result, description) => {
        if (description.type !== "attractions") {
            result.push(
                <div id={description.type}>
                    <br />
                    <div>{description.text}</div>
                </div>
            )
        }
        return result;
      }, [])
    
    return (
        <div className="descriptions">
            <div className="heading">About the Hotel</div>
            <div>{descriptionWithoutAttractions}</div>
        </div>
    )
}
