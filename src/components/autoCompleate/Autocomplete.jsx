import React, { Component, Fragment } from "react";
import PropTypes from "prop-types";
import { Input } from 'antd'
import Axios from 'axios'
class Autocomplete extends Component {
  static propTypes = {
    suggestions: PropTypes.instanceOf(Array)
  };

  static defaultProps = {
    suggestions: []
  };

  constructor(props) {
    super(props);
    this.state = {
      // The active selection's index
      activeSuggestion: 0,
      // The suggestions that match the user's input
      filteredSuggestions: [],
      // Whether or not the suggestion list is shown
      showSuggestions: false,
      // What the user has entered
      userInput: ""
    };
  }

  onChange = async event => {
    event.preventDefault();

    this.setState({userInput: event.target.value})
    let userInput = event.target.value

    
    let res = await Axios.get(`https://cors-anywhere.herokuapp.com/http://nexus.dev-env.vervotech.com/api/content/autosuggest?term=${userInput}`)
    
		if(res.data.status === "success"){
			this.setState({
        activeSuggestion: 0,
        filteredSuggestions: res.data.locations.slice(0, 5),
        showSuggestions: true,
        
      });
		}
    
  };

  onSelect = async () => {
    let res = await Axios.get(`http://nexus.dev-env.vervotech.com/api/content/autosuggest/popularLocations`)
    this.setState({
      activeSuggestion: 0,
      filteredSuggestions: res.data.locations.slice(0, 5),
      showSuggestions: true,
    });
  }

  onClick = e => {
    this.setState({
      activeSuggestion: 0,
      filteredSuggestions: [],
      showSuggestions: false,
      userInput: e.currentTarget.innerText
    });
    this.props.triggerParentLocationClick(e, e.currentTarget.innerText)
  };

  onKeyDown = e => {
    const { activeSuggestion, filteredSuggestions } = this.state;

    // User pressed the enter key
    if (e.keyCode === 13) {
      this.props.triggerParentLocation(filteredSuggestions[activeSuggestion].fullName);
      this.setState({
        activeSuggestion: 0,
        showSuggestions: false,
        userInput: filteredSuggestions[activeSuggestion].fullName
      });
      
    }
    // User pressed the up arrow
    else if (e.keyCode === 38) {
      if (activeSuggestion === 0) {
        return;
      }

      this.setState({ activeSuggestion: activeSuggestion - 1 });
    }
    // User pressed the down arrow
    else if (e.keyCode === 40 ) {
      if (activeSuggestion < filteredSuggestions.length -1) {
        this.setState( prevState => ({ activeSuggestion: prevState.activeSuggestion + 1 }) )
      }
      
    }
  };

  render() {
    const {
    
      onClick,
      onKeyDown,
      state: {
        activeSuggestion,
        filteredSuggestions,
        showSuggestions,
        userInput
      }
    } = this;
    const { triggerParentLocation } = this.props

    let suggestionsListComponent;

    if (showSuggestions && filteredSuggestions.length > 0) {
      if (filteredSuggestions.length) {
        suggestionsListComponent = (
          <ul className="suggestions">
            {filteredSuggestions.map((suggestion, index) => {
              let className;

              // Flag the active suggestion with a class
              if (index === activeSuggestion) {
                className = "suggestion-active";
              }

              return (
                <li className={className} key={suggestion.fullName} onClick={onClick}>
                  {suggestion.fullName}
                </li>
              );
            })}
          </ul>
        );
      } else {
        suggestionsListComponent = (
          <div class="no-suggestions">
            <em>No suggestions, you're on your own!</em>
          </div>
        );
      }
    }

    return (
      <div>
        <input

          type="text"
          placeholder="Destination"
          name="place"
          onChange={this.onChange}
          // onSelect={this.onSelect}
          onKeyDown={onKeyDown}
          value={this.state.userInput}
          autoComplete="off"
        />
        {suggestionsListComponent}
      </div>
    );
  }
}

export default Autocomplete;
